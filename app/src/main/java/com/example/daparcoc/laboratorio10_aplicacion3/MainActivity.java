package com.example.daparcoc.laboratorio10_aplicacion3;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public TextView outputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        outputText = (TextView) findViewById(R.id.textView2);
        fetchContacts();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void fetchContacts() {
        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI =
                ContactsContract.Contacts.CONTENT_URI;
        String _ID =
                ContactsContract.Contacts._ID;
        String DISPLAY_NAME =
                ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER =
                ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI =
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID =
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER =
                ContactsContract.CommonDataKinds.Phone.NUMBER;
        String TYPO =
                ContactsContract.CommonDataKinds.Phone.TYPE;
        Uri EmailCONTENT_URI =
                ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID =
                ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA =
                ContactsContract.CommonDataKinds.Email.DATA;
        StringBuffer output = new StringBuffer();
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI,
                null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String contact_id =
                        cursor.getString(cursor.getColumnIndex(_ID));
                String name =
                        cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(
                        cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    output.append("\n First Name:" + name);
                    Cursor phoneCursor = contentResolver.query(
                            PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        Integer phoneType = phoneCursor.getInt(
                                phoneCursor.getColumnIndex(TYPO));
                        String tipo="";
                        switch(phoneType){
                            case 1:
                                tipo="Casa";
                                break;
                            case 2:
                                tipo="Movil";
                                break;
                            default:
                                tipo="Otro";
                                break;
                        }
                        output.append("\n "+tipo+":" + phoneNumber);
                    }
                    phoneCursor.close();
                    Cursor emailCursor = contentResolver.query(
                            EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[] { contact_id }, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\nEmail:" + email);
                    }
                    emailCursor.close();
                }
                output.append("\n");
            }
            outputText.setText(output);
        }
        cursor.close();
    }

}


